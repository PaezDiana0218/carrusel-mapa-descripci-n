function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.685358, -74.106737], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let unMarcador = L.marker([4.685358, -74.106737
    ]);
    unMarcador.addTo(mapa);

   
}